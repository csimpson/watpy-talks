WebSockets - Using AsyncIO in Python 3 for Fun and Profit
=========================================================

**Or A Little Something I Learned at PyCon US 2015**

.. figure:: /_static/7389234452_b85fb1cf43_k.jpg
    :class: fill
 
    CC BY-SA https://www.flickr.com/photos/bigpresh/7389234452


Dorian Puła

@dorianpula

Presented on May 27th, 2015 @ WatPy


Inspiration
===========

.. image:: _static/talk_header_image.jpg
    :align: center

WebSockets from the Wire Up - Christine Spang
http://pyvideo.org/video/3506/websockets-from-the-wire-up


Intro to WebSockets
===================

.. rst-class:: build

- WebSocket is a protocol providing full-duplex communication channels over a single TCP connection. [Wikipedia]
- Used as a lightweight protocol for apps + webapps that need two-way communication.
- Spec: IETF RFC 6455

Server + Client
===============

.. figure:: /_static/websockets_screenshot.png
    :class: fill

Setup
=====

.. rst-class:: build

- asyncio
    - standard in Python 3.4
- websockets
    - PIP installable
    - Web: http://aaugustin.github.io/websockets/


Server
======

.. code-block:: python

    import asyncio
    import websockets

    @asyncio.coroutine
    def hello(websocket, path):
        name = yield from websocket.recv()  # Receiving
        print("< {}".format(name))

        greeting = "Hello {}!".format(name)
        yield from websocket.send(greeting)  # Sending
        print("> {}".format(greeting))

    start_server = websockets.serve(hello, 'localhost', 8765)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()


Client
======

.. code-block:: python

    import asyncio
    import websockets

    @asyncio.coroutine
    def hello():
        websocket = yield from websockets.connect('ws://localhost:8765/')

        name = input("What's your name? ")
        yield from websocket.send(name)  # Send
        print("> {}".format(name))

        greeting = yield from websocket.recv()  # Receive
        print("< {}".format(greeting))

    asyncio.get_event_loop().run_until_complete(hello())


Demo Time
=========

.. figure:: /_static/115781801_d11767a5a6_b.jpg
    :class: fill

    CC BY-SA https://www.flickr.com/photos/andih/115781801


In the Real World
=================

.. figure:: /_static/2692236683_1f3f8137f0_b.jpg
    :class: fill

    CC BY-SA https://www.flickr.com/photos/beraldoleal/2692236683/

Things to Keep in Mind
======================

.. rst-class:: build

- Browser support
- Need to setup proxy / load balancer / firewall
- WSGI is not for websockets
    - UWSGI
    - Gunicorn
- Use an event-loop framework
    - Tornado
    - Twisted


Integrations
============

- socket.io - http://socket.io/
- flask-sockets - https://github.com/kennethreitz/flask-sockets
- django-websockets


Thank You!
==========

.. figure:: /_static/7389234452_b85fb1cf43_k.jpg
    :class: fill
 
    CC BY-SA https://www.flickr.com/photos/bigpresh/7389234452

- Dorian Puła
- Twitter: @dorianpula
- Email: dorian.pula@gmail.com
- WWW: http://talks.dorianpula.ca/
